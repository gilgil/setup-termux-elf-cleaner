sudo apt install automake

git clone https://github.com/termux/termux-elf-cleaner.git

cd termux-elf-cleaner

git switch -c v3.0.1 # latest tag

aclocal # create aclocal.m4 and autom4te.cache/

# autoheader

automake --force-missing --add-missing

autoconf

./configure

make

sudo make install
